package com.example.farshid.intent;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    public static final String KEY_NAME="NAME";
    public static final String KEY_AGE="AGE";
    public static final String KEY_DISCREPTION="DISCREPTION";
    private EditText name,age,discreption;
    private Button send;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        name=(EditText) findViewById(R.id.name);
        age=(EditText) findViewById(R.id.age);
        discreption=(EditText) findViewById(R.id.description);

        send =( Button) findViewById(R.id.send);


        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MainActivity.this,ShowInfo.class);


//
                intent.putExtra(KEY_NAME,name.getText().toString());
                intent.putExtra(KEY_AGE,age.getText().toString());
                intent.putExtra(KEY_DISCREPTION,discreption.getText().toString());

                startActivity(intent);
            }
        });


    }
}
