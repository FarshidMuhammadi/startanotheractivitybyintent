package com.example.farshid.intent;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class ShowInfo extends AppCompatActivity {

    private TextView name,age,discreption;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_info);

        name=(TextView) findViewById(R.id.textViewName);
        age=(TextView) findViewById(R.id.textViewAge);
        discreption=(TextView) findViewById(R.id.textViewDiscreption);

        Intent intent=getIntent();

        name.setText(intent.getStringExtra(MainActivity.KEY_NAME));
        age.setText(intent.getStringExtra(MainActivity.KEY_AGE));
        discreption.setText(intent.getStringExtra(MainActivity.KEY_DISCREPTION));
    }
}
